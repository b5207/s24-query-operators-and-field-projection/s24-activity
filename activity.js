//SYNTAX: db.products.find({$and:[{name:{$regex:'y', $options: '$i'}}, {price:{$lt:10000}}]}, {"_id":0, "name":1, "price":1})
 //USERS
 db.users.find({$or:[{firstName:{$regex:'y', $options: '$i'}}, {lastName:{$regex:'y', $options: '$i'}}]}, {"_id":0, "email":1, "isAdmin":1})
 db.users.find({$and:[{firstName:{$regex:'e', $options: '$i'}}, {isAdmin:true}]}, {"_id":0, "email":1, "isAdmin":1})
 
 
 //PRODUCTS
 db.products.find({$and:[{name:{$regex:'x', $options: '$i'}}, {price:{$gte:50000}}]})
 //UPDATE
 db.products.updateMany({price:{$lt:2000}}, {$set: {"isActive": false}})
 //DELETE
 db.products.deleteMany({price:{$gt:20000}})